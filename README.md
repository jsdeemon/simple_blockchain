# Simple Blockchain project with Django 

```bash
$ virtualenv venv 
$ source venv/bin/acivate
$ deactivate
```

To run server
```bash
$ python3 manage.py runserver 
```

### Some info about blockchain 
Key Elements of Blockchain 
- Distributed ladger 
- Peer-to-peer network 
- Immutable records 
- Trust-less systems - trust the data, not the person 
- Transparency - the data is visible to everyone in the network 
- Provenance - possible to track the origin of every transaction inside the blockchain 


Centralized
Decentralized 
Distributed networks 

Decentralized netqork has a several individual entities that connected to each other 
In dinstributed network every node is independent and interconnected with each other 

Peer-to-Peer network is a group of computers that linked together with equal permissions and responsibilities for processing data  

Blockchain works on peer-to-peer networks 

Every blockchain is a distributed ledger 
But not every distributed ledger is a blockchain 
Blockchain is a form of distributed ledger 

Every blockhain is a database, but not every database is a blockchain 


TYPES OF BLOCKCHAIN 
3 types: 
- Public Blockchain - available for everyone, allows everyone to participate in blockchain 
- Private Blockchain - there is one network administrator, participants need concern a network administrator to join 
- Consortium Blockchain - instead of one organization multiple organization are present 
 So, in Private and Consortium blockhains participants need a permission to join a blockchain 
and everything works on the peer-to-peer network every node becomes in a peer-to-peer networks and becaomes a distributed ledger 

DIFFERENCE between public, private and consortion blockchains 

Who can join? 
Public - Anyone 
Private - Anyone in organization 
Consortium - selected nodes 

Read permissions 
Read permissions: 
Public - public 
Private - public or restricted 
Consortium - public or restricted 

Tamper data - modifying data 
Public - almost impossible 
Private - could be tempred 
Consortium - could be tempted 

Centralization? 
Public - No 
Private - Yes 
Consortium - Partial 

Consensus process ? 
Public - Permissionless 
Private - Requires permissions
Consortium - Requires permissions 